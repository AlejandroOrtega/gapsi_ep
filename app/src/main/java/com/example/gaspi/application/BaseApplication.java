package com.example.gaspi.application;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;

public class BaseApplication extends Application {

    private static RealmConfiguration configuration;
    public static Realm instance;

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        configuration = new RealmConfiguration.Builder()
                .name("Gaspi.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(configuration);

        try {
            instance = Realm.getInstance(configuration);
        }catch (RealmException e){
            if (instance != null)
                instance.deleteAll();
        }catch (RealmMigrationNeededException e){
            if (instance != null)
                instance.deleteAll();
        }
    }

    public static Realm getInstance() {
        return Realm.getInstance(configuration);
    }
    public static void closeInstance () {
        instance.close();
    }
    public static void deleteInstance () {
        try {
            Realm.deleteRealm(configuration);
        } catch (Exception e) {

        }
    }
}

package com.example.gaspi.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gaspi.R;
import com.example.gaspi.realmdatabase.ProductModel;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    private List<ProductModel> products;

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.historic_item, viewGroup, false);
        HistoryViewHolder viewHolder = new HistoryViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder historyViewHolder, int i) {
        historyViewHolder.product.setText(products.get(i).getProduct());
    }

    @Override
    public int getItemCount() {
        if (products != null) {
            return products.size();
        } else {
            return 0;
        }
    }

    public void setProducts(List<ProductModel> products) {
        this.products = products;
    }

    public static class HistoryViewHolder extends RecyclerView.ViewHolder{

        private TextView product;

        public HistoryViewHolder (View itemView) {
            super(itemView);
            this.product = itemView.findViewById(R.id.historicProductTextView);
        }
    }
}

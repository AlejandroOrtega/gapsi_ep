package com.example.gaspi.adapter;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gaspi.R;
import com.example.gaspi.response.ProductsResponse;
import com.example.gaspi.searchactivity.MainActivity;

import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {

    private List<ProductsResponse> products;
    private MainActivity mainActivity;

    @NonNull
    @Override
    public ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_item, viewGroup, false);
        ProductsViewHolder viewHolder = new ProductsViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsViewHolder productsViewHolder, int i) {

        productsViewHolder.title.setText(products.get(i).getProductDisplayName());
        productsViewHolder.price.setText(formatCurrency(products.get(i).getListPrice()));
        productsViewHolder.location.setText(getRealLocation(products.get(i).isMarketPlace() ? products.get(i).getMarketplaceSLMessage() : "N/A"));
        if(products.get(i).getThumbnail() == null && products.get(i).getSmImage() != null) {
            mainActivity.getImageByPresenter(products.get(i).getSmImage(), i);
        } else if(products.get(i).getThumbnail() != null) {
            productsViewHolder.thumbnail.setImageBitmap(products.get(i).getThumbnail());
        }
        //productsViewHolder.thumbnail.setImageDrawable(products.get(i).getThumbnail());
    }

    @Override
    public int getItemCount() {
        if (products != null) {
            return products.size();
        } else {
            return 0;
        }
    }

    private String getRealLocation(String location) {
        if(location.contains("V")) {
            return location.substring(0, location.indexOf("V"));
        } else {
            return location;
        }
    }

    public void setImage(int position, Bitmap image) {
        products.get(position).setThumbnail(image);
        //notifyItemChanged(position);
    }

    public void setProducts(List<ProductsResponse> products) {
        this.products = products;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    private String formatCurrency(double amount) {
        String price = String.valueOf(amount);
        if(!price.contains(".")) {
            price = price.concat(".00");
        } else if(price.endsWith(".0")) {
            price = price.concat("0");
        }
        price = "$ ".concat(price);
        return price;
    }

    public static class ProductsViewHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView price;
        private TextView location;
        private ImageView thumbnail;

        public ProductsViewHolder (View itemView) {
            super(itemView);
            this.title = itemView.findViewById(R.id.titleTextView);
            this.price = itemView.findViewById(R.id.priceTextView);
            this.location = itemView.findViewById(R.id.locationTextView);
            this.thumbnail = itemView.findViewById(R.id.thumbnailImageView);
        }
    }
}

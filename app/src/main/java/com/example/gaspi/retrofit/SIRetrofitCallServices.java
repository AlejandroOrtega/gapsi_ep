package com.example.gaspi.retrofit;

import android.net.Uri;
import android.util.Log;

import com.example.gaspi.BuildConfig;
import com.example.gaspi.response.FullResponse;
import com.example.gaspi.response.ProductsResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Callback;
import retrofit2.Response;

public class SIRetrofitCallServices {

    private static SIRetrofitCallServices instance;
    private SIRetrofitCallServicesCallback mCallback;
    private final boolean ONERROR = false;
    private final boolean ONSUCESS = true;


    public interface SIRetrofitCallServicesCallback {
        void onFinish(boolean isSuccess, String message, Object response);
    }

    private SIRetrofitCallServices() {
    }

    public SIRetrofitCallServices setCallback(SIRetrofitCallServicesCallback callback) {
        mCallback = callback;
        return this;
    }

    public static SIRetrofitCallServices getInstance() {
        if (instance == null) {
            return new SIRetrofitCallServices();
        } else {
            return instance;
        }
    }

    public SIRetrofitCallServices wsGetProducts(String product) {
        SIWSDataService service = SIRetrofitInstance.getRetrofitInstance(null).create(SIWSDataService.class);
        retrofit2.Call<FullResponse> call = service.getProducts(true, product, 1, 0);
        wsRetrofitResponseManage((retrofit2.Call) call);

        return this;
    }

    private void wsRetrofitResponseManage(retrofit2.Call<Object> callRetrofit) {
        callRetrofit.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(retrofit2.Call<Object> call, Response<Object> response) {
                Log.i("REQUEST", "REQUEST : " + response.raw().request().toString());
                String jsonInString = new Gson().toJson(response);
                Log.i("REQUEST", "REPONSE>>> " + jsonInString);
                if (mCallback != null) {
                    if (response.body() == null) {
                        mCallback.onFinish(ONERROR, response.raw().message().toString(), null);
                    } else {
                        mCallback.onFinish(ONSUCESS, "", response.body());
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<Object> call, Throwable t) {
                Log.e("REQUEST", t.getMessage().toString());
                if (mCallback != null) {
                    mCallback.onFinish(ONERROR, t.getMessage().toString(), null);
                }
            }
        });
    }
}

package com.example.gaspi.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SIRetrofitInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = "https://shoppapp.liverpool.com.mx";

    public static Retrofit getRetrofitInstance(String baseURL) {

        if(baseURL == null){
            baseURL = BASE_URL;
        }
        OkHttpClient okHttpClientunsafe = SIUnsafeOKHttpClient.getUnsafeOkHttpClient();
        retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(baseURL)
                .client(okHttpClientunsafe)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
}

package com.example.gaspi.retrofit;

import com.example.gaspi.response.FullResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SIWSDataService {

    @GET("/appclienteservices/services/v3/plp")
    Call<FullResponse> getProducts(@Query("force-plp") boolean forcePlp, @Query("search-string") String product, @Query("page-number") int pageNumber, @Query("number-of-items-per-page") int items);
}

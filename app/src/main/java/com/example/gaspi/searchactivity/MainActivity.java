package com.example.gaspi.searchactivity;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.gaspi.R;
import com.example.gaspi.adapter.HistoryAdapter;
import com.example.gaspi.adapter.ProductsAdapter;
import com.example.gaspi.realmdatabase.ProductModel;
import com.example.gaspi.response.ProductsResponse;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MainPresenter mainPresenter;

    private EditText        productEditText;
    private LinearLayout    historicLayout;
    private LinearLayout    productsLayout;
    private RecyclerView    historyRecyclerView;
    private RecyclerView    productRecyclerView;
    private TextView        withoutProductsTextView;

    private ProductsAdapter productsAdapter;
    private HistoryAdapter  historyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainPresenter = new MainPresenter();
        mainPresenter.setMainActivity(this);

        productEditText = findViewById(R.id.productEditText);
        historicLayout = findViewById(R.id.historyLayout);
        productsLayout = findViewById(R.id.productsLayout);
        historyRecyclerView = findViewById(R.id.historyRecyclerView);
        productRecyclerView = findViewById(R.id.productsRecyclerView);
        withoutProductsTextView = findViewById(R.id.withoutProductsTextView);

        productRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        productRecyclerView.setItemAnimator(new DefaultItemAnimator());

        historyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        historyRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mainPresenter.getHistory();

        productEditText.setOnFocusChangeListener(onFocusChangeListener);
    }

    public void getImageByPresenter(String url, int position) {
        mainPresenter.callWsGetImage(url, position);
    }

    public void setImageInAdapter(int position, Bitmap image) {
        productsAdapter.setImage(position, image);
        productsAdapter.notifyDataSetChanged();
    }

    private View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if(b) {
                mainPresenter.getHistory();
            }
        }
    };

    public void onClickSearch(View view){
        productEditText.clearFocus();
        historicLayout.setVisibility(View.GONE);
        withoutProductsTextView.setVisibility(View.VISIBLE);
        productsLayout.setVisibility(View.GONE);
        mainPresenter.callWsGetProducts(productEditText.getText().toString());
    }

    public void setError(String error) {
        withoutProductsTextView.setText(error);
    }

    public void setHistoricProducts(List<ProductModel> productModels) {
        historicLayout.setVisibility(View.VISIBLE);
        if(historyRecyclerView.getAdapter() == null) {
            historyAdapter = new HistoryAdapter();
            historyAdapter.setProducts(productModels);
            historyRecyclerView.setAdapter(historyAdapter);
        } else {
            historyAdapter.setProducts(productModels);
            historyAdapter.notifyDataSetChanged();
        }
    }

    public void setProducts(List<ProductsResponse> products) {
        withoutProductsTextView.setVisibility(View.GONE);
        productsLayout.setVisibility(View.VISIBLE);

        if(productRecyclerView.getAdapter() == null) {
            productsAdapter = new ProductsAdapter();
            productsAdapter.setMainActivity(this);
            productsAdapter.setProducts(products);
            productRecyclerView.setAdapter(productsAdapter);
        } else {
            productsAdapter.setProducts(products);
            productsAdapter.notifyDataSetChanged();
        }
    }
}

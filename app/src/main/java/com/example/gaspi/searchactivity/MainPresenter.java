package com.example.gaspi.searchactivity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.example.gaspi.realmdatabase.ProductsDao;
import com.example.gaspi.response.FullResponse;
import com.example.gaspi.response.ProductsResponse;
import com.example.gaspi.retrofit.SIRetrofitCallServices;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainPresenter {

    private MainActivity mainActivity;
    private Context context;
    private ProductsDao productsDao = new ProductsDao();

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        context = mainActivity;
    }

    public void callWsGetImage(final String url, final int position) {

        final Handler handler = new Handler(Looper.getMainLooper());
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        InputStream inputStream = response.body().byteStream();
                        mainActivity.setImageInAdapter(position, BitmapFactory.decodeStream(inputStream));
                    }
                });

            }
        });
    }

    public void callWsGetProducts(final String product) {

        SIRetrofitCallServices.getInstance()
                .setCallback(new SIRetrofitCallServices.SIRetrofitCallServicesCallback() {

                    @Override
                    public void onFinish(boolean isSuccess, String message, Object response) {
                        if (isSuccess) {
                            if ((response) != null) {
                                FullResponse fullResponse = (FullResponse) response;

                                if(fullResponse.getStatus().getStatusCode() == 0) {
                                    productsDao.insertProduct(product);
                                    List<ProductsResponse> productsResponseList = fullResponse.getPlpResults().getRecords();
                                    mainActivity.setProducts(productsResponseList);
                                } else {
                                    mainActivity.setError(fullResponse.getStatus().getStatus());
                                }


                            } else {
                                mainActivity.setError("No se encontraron resultados para ese producto");
                            }
                        } else {
                            mainActivity.setError("Inténtelo nuevamente en un momento");
                        }
                    }
                })
        .wsGetProducts(product);
    }

    public void getHistory() {

        if(productsDao.queryProducts().size() > 0) {
            mainActivity.setHistoricProducts(productsDao.queryProducts());
        }
    }
}

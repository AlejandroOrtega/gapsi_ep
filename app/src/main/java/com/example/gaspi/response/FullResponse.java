package com.example.gaspi.response;

public class FullResponse {
    private StatusResponse status;
    private PlpResultsResponse plpResults;

    public StatusResponse getStatus() {
        return status;
    }

    public PlpResultsResponse getPlpResults() {
        return plpResults;
    }
}

package com.example.gaspi.response;

import android.graphics.Bitmap;

public class ProductsResponse {

    private String productDisplayName;
    private double listPrice;
    private boolean isMarketPlace;
    private String marketplaceSLMessage;
    private String smImage;
    private Bitmap thumbnail;

    public String getProductDisplayName() {
        return productDisplayName;
    }

    public double getListPrice() {
        return listPrice;
    }

    public boolean isMarketPlace() {
        return isMarketPlace;
    }

    public String getMarketplaceSLMessage() {
        return marketplaceSLMessage;
    }

    public String getSmImage() {
        return smImage;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }
}

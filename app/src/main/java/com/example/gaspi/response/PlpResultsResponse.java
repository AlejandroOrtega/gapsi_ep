package com.example.gaspi.response;

import java.util.List;

public class PlpResultsResponse {
    private List<ProductsResponse> records;

    public List<ProductsResponse> getRecords() {
        return records;
    }
}

package com.example.gaspi.response;

public class StatusResponse {
    String status;
    int statusCode;

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }
}

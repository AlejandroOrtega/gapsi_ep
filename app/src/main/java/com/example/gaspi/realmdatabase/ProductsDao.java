package com.example.gaspi.realmdatabase;

import com.example.gaspi.application.BaseApplication;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class ProductsDao {

    public void insertProduct(String product) {

        ProductModel productModel;
        RealmResults<ProductModel> result = null;
        Realm realm = BaseApplication.getInstance();

        if (realm != null) {

            result = realm.where(ProductModel.class).equalTo("product", product).findAll();

            if(result == null || result.size() == 0){
                realm.beginTransaction();

                productModel = realm.createObject(ProductModel.class);

                productModel.setProduct(product);

                realm.commitTransaction();
            }

            realm.close();
        }
    }

    public List<ProductModel> queryProducts() {

        List<ProductModel> productModels = new ArrayList<>();
        RealmResults<ProductModel> result = null;
        Realm realm = BaseApplication.getInstance();
        if (realm != null) {
            result = realm.where(ProductModel.class).findAll();
            productModels = realm.copyFromRealm(result);
            realm.close();
        }
        return productModels;
    }
}

package com.example.gaspi.realmdatabase;

import io.realm.RealmObject;

public class ProductModel extends RealmObject {
    String product;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
